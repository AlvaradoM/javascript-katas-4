const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";
const lotrCitiesArray = ["Mordor","Gondor","Rohan","Beleriand","Mirkwood","Dead Marshes","Rhun","Harad"];
const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

function addToPage(text) {
    const newDiv = document.createElement('div')
    newDiv.textContent = text
    container.appendChild(newDiv)
}

function katas1() {
    addToPage(`one: ${JSON.stringify(gotCitiesCSV.split(','))}`)
}
katas1()

function katas2() {
    addToPage(`two: ${JSON.stringify(bestThing.split(' '))}`)
}
katas2()

function katas3() {
    let re = /,/gi
    addToPage(`three: ${JSON.stringify(gotCitiesCSV.replace(re, '; '))}`)
}
katas3()

function katas4() {
    addToPage(`four: ${JSON.stringify(lotrCitiesArray.join(', '))}`)
}
katas4()

function katas5() {
    addToPage(`five: ${JSON.stringify(lotrCitiesArray.slice(0,5))}`)
}
katas5()

function katas6() {
    addToPage(`six: ${JSON.stringify(lotrCitiesArray.slice(3,8))}`)
}
katas6()

function katas7() {
    addToPage(`seven: ${JSON.stringify(lotrCitiesArray.slice(2,5))}`)
}
katas7()

function katas8() {
    lotrCitiesArray.splice(2,1)
    addToPage(`eight: ${JSON.stringify(lotrCitiesArray)}`)
}
katas8()

function katas9() {
    lotrCitiesArray.splice(5,3)
    addToPage(`nine: ${JSON.stringify(lotrCitiesArray)}`)
}
katas9()

function katas10() {
    lotrCitiesArray.splice(2,0,'Rohan')
    addToPage(`ten: ${JSON.stringify(lotrCitiesArray)}`)
}
katas10()

function katas11() {
    lotrCitiesArray.splice(5,1,'Deadest Marshes')
    addToPage(`eleven: ${JSON.stringify(lotrCitiesArray)}`)
}
katas11()

function katas12() {
    addToPage(`twelve: ${JSON.stringify(bestThing.slice(0,14))}`)
}
katas12()

function katas13() {
    addToPage(`thirteen: ${JSON.stringify(bestThing.slice(-12))}`)
}
katas13()

function katas14() {
    addToPage(`fourteen: ${JSON.stringify(bestThing.slice(23,38))}`)
}
katas14()

function katas15() {
    addToPage(`fifthteen: ${JSON.stringify(bestThing.substring(bestThing.length - 12))}`)
}
katas15()

function katas16() {
    addToPage(`sixteen: ${JSON.stringify(bestThing.substring(23,38))}`)
}
katas16()

function kata17() {
    addToPage(`seventeen: ${JSON.stringify(bestThing.indexOf('only'))}`)
}
kata17()

function kata18() {
    addToPage(`eighteen: ${JSON.stringify(bestThing.indexOf('bit'))}`)
}
kata18()

function kata19() {
    let newArr = gotCitiesCSV.split(',')
    const arr = []
    const vowels = ['aa', 'ee', 'ii', 'oo', 'uu']
    for (let city of newArr) {
        for (let vowel of vowels) {
            if (city.includes(vowel)) {
                arr.push(city)
        }
        }
    }
    addToPage(`nineteen: ${JSON.stringify(arr)}`)
}
kata19()

function kata20() {
    const arr = []
    const contain = ['or']
    for (let city of lotrCitiesArray) {
        if (city.endsWith(contain)) {
            arr.push(city)
        }
    }
    addToPage(`twenty: ${JSON.stringify(arr)}`)
}
kata20()

function kata21() {
    const newArr = bestThing.split(' ')
    const arr = []
    for (let word of newArr) {
        if (word.startsWith('b')) {
            arr.push(word)
        }
    }
    addToPage(`twentyOne: ${JSON.stringify(arr)}`)
}
kata21()

function kata22() {
    if (lotrCitiesArray.includes('Mirkwood')) {
        response = 'Yes'
    } else {
        response = 'No'
    }
    addToPage(`twentyTwo: ${JSON.stringify(response)}`)
}
kata22()

function kata23() {
    if (lotrCitiesArray.includes('Hollywood')) {
        response = 'Yes'
    } else {
        response = 'No'
    }
    addToPage(`twentyThree: ${JSON.stringify(response)}`)
}
kata23()

function kata24() {
    addToPage(`twentyFour: ${JSON.stringify(lotrCitiesArray.indexOf('Mirkwood'))}`)
}
kata24()

function kata25() {
    const arr = []
    for (let name of lotrCitiesArray) {
        if (name.includes(' ')) {
            arr.push(name)
        }
    }
    addToPage(`twentyFive: ${JSON.stringify(arr)}`)
}
kata25()

function kata26() {
    addToPage(`twentySix: ${JSON.stringify(lotrCitiesArray.reverse())}`)
}
kata26()

function kata27() {
    addToPage(`twentySeven: ${JSON.stringify(lotrCitiesArray.sort())}`)
}
kata27()

function kata28() {
    lotrCitiesArray.sort(function(a,b) {
        return a.length - b.length
    })
    addToPage(`twentyEight: ${JSON.stringify(lotrCitiesArray)}`)
}
kata28()

const lastCity = lotrCitiesArray.pop()

function kata29() {
    addToPage(`twentyNine: ${JSON.stringify(lotrCitiesArray)}`)
}
kata29()

function kata30() {
    lotrCitiesArray.push(lastCity)
    addToPage(`thirty: ${JSON.stringify(lotrCitiesArray)}`)
}
kata30()

const firstCity = lotrCitiesArray.shift()

function kata31() {
    addToPage(`thirtyOne: ${JSON.stringify(lotrCitiesArray)}`)
}
kata31()

function kata32() {
    lotrCitiesArray.unshift(firstCity)
    addToPage(`thirtyTwo: ${JSON.stringify(lotrCitiesArray)}`)
}
kata32()
